Role Name
=========

Установка Elascticsearch

Requirements
------------



Role Variables
--------------
Версия elasticsearch
elasticsearch_version: '7.x'
elasticsearch_package: elasticsearch
elasticsearch_package_state: present

Установка времени
time_zone: Europe/Moscow

# elasticsearch_service_state: started
# elasticsearch_service_enabled: true

С каких хостов принимать логи и по какому порту
elasticsearch_network_host: 0.0.0.0
elasticsearch_http_port: 9200

Dependencies
------------



Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - elasticsearch

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
