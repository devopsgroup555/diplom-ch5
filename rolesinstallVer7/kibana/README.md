Role Name
=========
Уставновка Kibana

Requirements
------------


Role Variables
--------------
kibana_version: "7.x"
kibana_package: kibana
kibana_package_state: present

kibana_server_port: 5601
kibana_server_host: "0.0.0.0"

Dependencies
------------

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - kibana

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
