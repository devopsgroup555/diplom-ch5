Role Name
=========

Установка Filebeat 

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------

filebeat_version: 7.x
filebeat_package: filebeat
filebeat_package_state: present

Адрес elasticserch и logstash
filebeat_output_elasticsearch_hosts: "http://10.10.99.155:9200"
filebeat_output_logstash_hosts: "10.10.99.155:5044"

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - filebeat

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
