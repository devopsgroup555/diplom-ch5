Role Name
=========

Установка NGINX для ELK

Requirements
------------
Nginx c nginx.conf c проксирование на сервер Kibana
Certbot - генерация сертификата 

Role Variables
--------------

nginx_package_name: "nginx"
time_zone: Europe/Moscow

Установка имени сайта и адрес где устновлена Kibana
server_name_site: kibana.compute.gq
proxy_pass_address: "http://10.10.99.155:5601"

Генерация сертификата SSL для сайта (letsencrypt)
letsencrypt_email: passwods@gmail.com
domain_name: kibana.compute.gq
wwwdomain_name: www.kibana.compute.gq

Dependencies
------------



Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - nginx

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
